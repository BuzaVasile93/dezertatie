﻿using System;

namespace Pharmalogic.Pilot.Dal.Interfaces.Base
{
    public interface IUnitOfWork :IDisposable
    {
        object Context { get; }
        void BeginTransaction();
        void Commit();
    }
}
