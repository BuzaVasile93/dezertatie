﻿using System.Collections.Generic;

namespace Pharmalogic.Pilot.Dal.Interfaces.Base
{
    public interface IRepository<T> : IReadOnlyRepository<T> where T : class
    {
        void Delete(T entity);
        void Delete(IList<T> entities);
        void Delete(object id);
        void Save(T entity);
        void Save(IList<T> entities);
        void Update(T entity);
        void Update(IList<T> entities);
        void SaveChanges();
        void ExecuteUpdate(string hql);
        void Refresh(T instance);
        IEnumerable<T> ExecuteHql(string hql);
        IEnumerable<T> Query();
        IUnitOfWork UnitOfWork { get; }
    }
}