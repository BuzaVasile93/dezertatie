﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Pharmalogic.Pilot.Dal.Interfaces.Base
{
    public interface IReadOnlyRepository<T> :IDisposable where T : class
    {
        IQueryable<T> AsQueryable();
        IQueryable<T> AsQueryable(Expression<Func<T, bool>> where);
        IQueryable<T> AsExpandable();
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> where);
        T GetById(object id);
        T SingleOrDefault(Expression<Func<T, bool>> where);
        T FirstOrDefault(Expression<Func<T, bool>> where);
        int Count();
        int Count(Expression<Func<T, bool>> where);
    }
}