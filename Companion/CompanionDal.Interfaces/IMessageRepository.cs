﻿using CompanionDal.Models;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace CompanionDal.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
    }
}
