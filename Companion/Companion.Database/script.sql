USE [master]
GO
/****** Object:  Database [Companion]    Script Date: 4/19/2017 6:02:38 PM ******/
CREATE DATABASE [Companion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Companion', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.VBUZASQL\MSSQL\DATA\Companion.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Companion_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.VBUZASQL\MSSQL\DATA\Companion_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Companion] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Companion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Companion] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Companion] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Companion] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Companion] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Companion] SET ARITHABORT OFF 
GO
ALTER DATABASE [Companion] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Companion] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Companion] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Companion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Companion] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Companion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Companion] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Companion] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Companion] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Companion] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Companion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Companion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Companion] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Companion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Companion] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Companion] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Companion] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Companion] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Companion] SET  MULTI_USER 
GO
ALTER DATABASE [Companion] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Companion] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Companion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Companion] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Companion] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Companion]
GO
/****** Object:  Table [dbo].[Demand]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Demand](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[UserType] [nchar](10) NOT NULL,
	[StartingPoint] [nchar](10) NOT NULL,
	[StopPoint] [nvarchar](50) NOT NULL,
	[Message] [nvarchar](50) NOT NULL,
	[CarPlace] [int] NOT NULL,
 CONSTRAINT [PK_Demand] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[History]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[UserType] [nchar](10) NOT NULL,
	[Rating] [int] NULL,
	[Comment] [nvarchar](50) NULL,
	[StartingPoint] [nvarchar](50) NOT NULL,
	[StopPoint] [nvarchar](50) NULL,
	[Adress] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_History] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Message]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ReceptorId] [int] NULL,
	[UserType] [nchar](10) NULL,
	[TitleMessage] [nvarchar](50) NULL,
	[Message] [nvarchar](300) NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ReceptorId] [int] NOT NULL,
	[Message] [nvarchar](50) NOT NULL,
	[Agreement] [int] NOT NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rating]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rating](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Rating] [int] NULL,
	[Comment] [nvarchar](300) NULL,
 CONSTRAINT [PK_Rating_1] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleClaims]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[ClaimType] [nvarchar](50) NOT NULL,
	[ClaimValue] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 4/19/2017 6:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NULL,
	[Email] [nvarchar](256) NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
	[SecurityStamp] [nvarchar](max) NOT NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[CarType] [nvarchar](50) NULL,
	[CarNr] [nvarchar](50) NULL,
	[Message] [nvarchar](300) NULL,
	[Experince] [int] NULL,
	[Photo] [nvarchar](500) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Demand]  WITH CHECK ADD  CONSTRAINT [FK_Demand_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Demand] CHECK CONSTRAINT [FK_Demand_Users]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_Message] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_Message]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_Users]
GO
ALTER TABLE [dbo].[RoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_UserClaims_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[RoleClaims] CHECK CONSTRAINT [FK_UserClaims_Roles]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users]
GO
USE [master]
GO
ALTER DATABASE [Companion] SET  READ_WRITE 
GO
