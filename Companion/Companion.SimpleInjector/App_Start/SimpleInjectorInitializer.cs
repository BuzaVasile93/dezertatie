using System.Reflection;
using System.Web.Mvc;
using Companion.Log;
using CompanionBl;
using CompanionBl.Interfaces;
using CompanionDal.Interfaces;
using CompanionDAL.Repositories;
using Pharmalogic.Pilot.Dal.Base;
using Pharmalogic.Pilot.Dal.Interfaces.Base;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;


namespace Companion.SimpleInjector.App_Start
{

  
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {

            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<ILogger, Logger>(Lifestyle.Scoped);


            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
            container.Register<IDemandRepository,DemandRepository>(Lifestyle.Scoped);
            container.Register<IHistoryRepository, HistoryRepository>(Lifestyle.Scoped);
            container.Register<IMessageRepository, MessageRepository>(Lifestyle.Scoped);
            container.Register<INotificationRepository, NotificationRepository>(Lifestyle.Scoped);
            container.Register<IRatingRepository,RatingRepository>(Lifestyle.Scoped);
            container.Register<IRoleRepository,RoleRepository>(Lifestyle.Scoped);


            container.Register<IUserService, UserService>(Lifestyle.Scoped);
            container.Register<IDemandService, DemandService>(Lifestyle.Scoped);
            container.Register<IHistoryService, HisotyService>(Lifestyle.Scoped);
            container.Register<IMessageService, MessageService>(Lifestyle.Scoped);
            container.Register<INotificationService, NotificationService>(Lifestyle.Scoped);
            container.Register<IRatingService, RatingService>(Lifestyle.Scoped);
            container.Register<ICompanionSecurityService, CompanionSecurityService>(Lifestyle.Scoped);
            container.Register<IRoleService,RoleService>(Lifestyle.Scoped);


        }
    }
}