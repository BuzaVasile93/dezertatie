using CompanionDal.Models;
using ConsoleApplication1.Models.Mapping;

namespace CompanionDal.Model
{
    using System.Data.Entity;

    public partial class CompanionDB : DbContext
    {
        static CompanionDB()
        {
            Database.SetInitializer<CompanionDB>(null);
        }
        public CompanionDB()
            : base("name=CompanionDB")
        {
        }

        public DbSet<Demand> Demands { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<RoleClaim> RoleClaims { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DemandMap());
            modelBuilder.Configurations.Add(new HistoryMap());
            modelBuilder.Configurations.Add(new MessageMap());
            modelBuilder.Configurations.Add(new NotificationMap());
            modelBuilder.Configurations.Add(new RatingMap());
            modelBuilder.Configurations.Add(new RoleClaimsMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserMap());



        }

    }
}
