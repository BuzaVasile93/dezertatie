﻿using Companion.Log;
using CompanionDal.Interfaces;
using CompanionDal.Models;
using Pharmalogic.Pilot.Dal.Base;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace CompanionDAL.Repositories
{
   public class RatingRepository:Repository<Rating>, IRatingRepository
    {
        public RatingRepository(IUnitOfWork unitOfWork, ILogger logger): base(unitOfWork, logger)
        {
           
        }
    }
}
