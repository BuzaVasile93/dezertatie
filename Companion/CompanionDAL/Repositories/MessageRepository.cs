﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Companion.Log;
using CompanionDal.Interfaces;
using CompanionDal.Models;
using Pharmalogic.Pilot.Dal.Base;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace CompanionDAL.Repositories
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        public MessageRepository(IUnitOfWork unitOfWork, ILogger logger) : base(unitOfWork, logger)
        {

        }

    }
}