﻿using Companion.Log;
using CompanionDal.Interfaces;
using CompanionDal.Models;
using Pharmalogic.Pilot.Dal.Base;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace CompanionDAL.Repositories
{
   public class DemandRepository : Repository<Demand>, IDemandRepository
    {
        public DemandRepository(IUnitOfWork unitOfWork, ILogger logger): base(unitOfWork, logger)
        {
           
        }
    }
}
