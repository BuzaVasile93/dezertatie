using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class RoleClaimsMap : EntityTypeConfiguration<RoleClaim>
    {
        public RoleClaimsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ClaimType)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ClaimValue)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("RoleClaims");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.ClaimType).HasColumnName("ClaimType");
            this.Property(t => t.ClaimValue).HasColumnName("ClaimValue");

            // Relationships
            this.HasRequired(t => t.Role)
                .WithMany(t => t.RoleClaims)
                .HasForeignKey(d => d.RoleId);
        }
    }
}
