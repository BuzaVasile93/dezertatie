using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class RatingMap : EntityTypeConfiguration<Rating>
    {
        public RatingMap()
        {

            // Primary Key
            this.HasKey(t => t.RecordId);

            // Properties
            this.Property(t => t.Comment)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("Rating");
            this.Property(t => t.RecordId).HasColumnName("RecordId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Rating1).HasColumnName("Rating");
            this.Property(t => t.Comment).HasColumnName("Comment");
        }
    }
}
