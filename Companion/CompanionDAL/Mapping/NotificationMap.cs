using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class NotificationMap : EntityTypeConfiguration<Notification>
    {
        public NotificationMap()
        {
            // Primary Key
            this.HasKey(t => t.RecordId);

            // Properties
            this.Property(t => t.Message)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Notifications");
            this.Property(t => t.RecordId).HasColumnName("RecordId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.ReceptorId).HasColumnName("ReceptorId");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Agreement).HasColumnName("Agreement");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Notifications)
                .HasForeignKey(d => d.UserId);
        }
    }
}
