using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class DemandMap : EntityTypeConfiguration<Demand>
    {
        public DemandMap()
        {
            // Primary Key
            this.HasKey(t => t.RecordId);

            // Properties
            this.Property(t => t.UserType)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.StartingPoint)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.StopPoint)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Message)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Demand");
            this.Property(t => t.RecordId).HasColumnName("RecordId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.StartingPoint).HasColumnName("StartingPoint");
            this.Property(t => t.StopPoint).HasColumnName("StopPoint");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.CarPlace).HasColumnName("CarPlace");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Demands)
                .HasForeignKey(d => d.UserId);

        }
    }
}
