using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(t => t.PasswordHash)
                .IsRequired();

            this.Property(t => t.SecurityStamp)
                .IsRequired();

            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CarType)
                .HasMaxLength(50);

            this.Property(t => t.CarNr)
                .HasMaxLength(50);

            this.Property(t => t.Message)
                .HasMaxLength(300);

            this.Property(t => t.Photo)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Users");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.PasswordHash).HasColumnName("PasswordHash");
            this.Property(t => t.SecurityStamp).HasColumnName("SecurityStamp");
            this.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber");
            this.Property(t => t.LockoutEndDateUtc).HasColumnName("LockoutEndDateUtc");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.CarType).HasColumnName("CarType");
            this.Property(t => t.CarNr).HasColumnName("CarNr");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Experince).HasColumnName("Experince");
            this.Property(t => t.Photo).HasColumnName("Photo");
        }
    }
}
