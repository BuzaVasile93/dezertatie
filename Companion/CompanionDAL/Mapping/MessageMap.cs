using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            // Primary Key
            this.HasKey(t => t.RecordId);

            // Properties
            this.Property(t => t.UserType)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.TitleMessage)
                .HasMaxLength(50);

            this.Property(t => t.Message1)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("Message");
            this.Property(t => t.RecordId).HasColumnName("RecordId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.ReceptorId).HasColumnName("ReceptorId");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.TitleMessage).HasColumnName("TitleMessage");
            this.Property(t => t.Message1).HasColumnName("Message");
            this.Property(t => t.Date).HasColumnName("Date");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Messages)
                .HasForeignKey(d => d.UserId);

        }
    }
}
