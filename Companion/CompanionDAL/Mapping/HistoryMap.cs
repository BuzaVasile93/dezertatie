using System.Data.Entity.ModelConfiguration;
using CompanionDal.Models;

namespace ConsoleApplication1.Models.Mapping
{
    public class HistoryMap : EntityTypeConfiguration<History>
    {
        public HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.RecordId);

            // Properties
            this.Property(t => t.UserType)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Comment)
                .HasMaxLength(50);

            this.Property(t => t.StartingPoint)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.StopPoint)
                .HasMaxLength(50);

            this.Property(t => t.Adress)
                .HasMaxLength(50);

            this.Property(t => t.PhoneNumber)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("History");
            this.Property(t => t.RecordId).HasColumnName("RecordId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.Rating).HasColumnName("Rating");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.StartingPoint).HasColumnName("StartingPoint");
            this.Property(t => t.StopPoint).HasColumnName("StopPoint");
            this.Property(t => t.Adress).HasColumnName("Adress");
            this.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber");
        }
    }
}
