﻿using System;
using System.Data.Entity;
using CompanionDal.Model;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace Pharmalogic.Pilot.Dal.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;
        private bool _disposed;
//        private ITransaction _transaction;

        public UnitOfWork()
        {
            _context = new CompanionDB();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing && _context != null)
            {
                _context.Dispose();
                _disposed = true;
            }
        }

        public object Context
        {
            get { return _context; }
        }

        public void BeginTransaction()
        {
//            if (!_context.Transaction.IsActive)
//            {
//                _transaction = _context.BeginTransaction();
//            }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
