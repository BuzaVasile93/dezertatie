﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel.Channels;
using Companion.Log;
using CompanionDal.Model;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace Pharmalogic.Pilot.Dal.Base
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected ILogger Log;
        private bool _disposed;
        private IUnitOfWork _unitOfWork;
        private DbContext _session;

        /// <summary>
        /// Constructs a new repository instance.
        /// </summary>
        /// <param name="log">The log.</param>
        protected Repository(IUnitOfWork unitOfWork, ILogger log)
        {
            Log = log;
            _session = unitOfWork.Context as DbContext;
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets a reference to the data context associated with this instance.
        /// </summary>
        public DbContext Session
        {
            set { _session = value; }
            get { return _session ?? (_session = new CompanionDB()); }
        }

        public void Refresh(T instance)
        {
//            Session.Refresh(instance);
        }

        #region Implementation of IReadOnlyRepository<T>

        /// <summary>
        /// Converts a generic IEnumerable<T> to a generic IQueryable<T>.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> AsQueryable()
        {
            return Session.Set<T>();
        }

        /// <summary>
        /// Converts a generic IEnumerable<T> to a generic IQueryable<T> that is obtained 
        /// by invoking a projection function on each element of the input sequence.
        /// </summary>
        /// <param name="where">A function to test each element for a condition.</param>
        /// <returns></returns>
        public IQueryable<T> AsQueryable(Expression<Func<T, bool>> where)
        {
            return Session.Set<T>().Where(where);
        }

        /// <summary>
        /// Converts a generic IQueryable<T> to a generic IQueryable<T> from the Linqkit lib.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> AsExpandable()
        {
//            return this.AsQueryable().AsExpandable();
            return this.AsQueryable();
        }

        /// <summary>
        /// Gets a sequence of all elements.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll()
        {
            return Session.Set<T>().ToList();
        }

        /// <summary>
        /// Filters a sequence of values based on a predicate.
        /// </summary>
        /// <param name="where">A function to test each element for a condition.</param>
        /// <returns></returns>
        public IEnumerable<T> GetAll(Expression<Func<T, bool>> where)
        {
            return Session.Set<T>().Where(where).ToList();
        }

        public virtual T GetById(object id)
        {
            return Session.Set<T>().Find(id);
        }

        /// <summary>
        /// Returns the only element of a sequence, or a default value if the sequence is empty; 
        /// this method throws an exception if there is more than one element in the sequence.
        /// </summary>
        /// <param name="where">A function to test each element for a condition.</param>
        /// <returns></returns>
        public T SingleOrDefault(Expression<Func<T, bool>> where)
        {
            return Session.Set<T>().Where(where).SingleOrDefault();
        }

        /// <summary>
        /// Returns the first element of a sequence, or a default value if the sequence contains no elements.
        /// </summary>
        /// <param name="where">A function to test each element for a condition.</param>
        /// <returns></returns>
        public T FirstOrDefault(Expression<Func<T, bool>> where)
        {
            return Session.Set<T>().Where(where).FirstOrDefault();
        }

        /// <summary>
        /// Returns the number of elements in a sequence.
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return Session.Set<T>().Count();
        }

        /// <summary>
        /// Returns the number of elements in the specified sequence that satisfies a condition.
        /// </summary>
        /// <param name="where">A function to test each element for a condition.</param>
        /// <returns></returns>
        public int Count(Expression<Func<T, bool>> where)
        {
            return AsQueryable(where).Count();
        }

        #endregion

        #region Implementation of IRepository<T>

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The operation info.</returns>
        public virtual void Delete(T entity)
        {
//            try
//            {
//                Session.(entity);
//                SaveChanges();
//
//            }
//            catch (Exception e)
//            {
//
//                Log.Error(e.Message);
//            }
        }

        /// <summary>
        /// Deletes the specified entities.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <returns>The operation info.</returns>
        public virtual void Delete(IList<T> entities)
        {
//
//            try
//            {
//                for (int i = 0; i < entities.Count; i++)
//                {
//                    Session.Delete(entities[i]);
//                }
//
//                SaveChanges();
//            }
//            catch (Exception e)
//            {
//                Log.Error(e.Message);
//            }
        }

        public virtual void Delete(object id)
        {

//            try
//            {
//                string queryString = string.Format("delete {0} where id = :id",
//                                                   typeof(T));
//                _session.CreateQuery(queryString)
//                    .SetParameter("id", id)
//                    .ExecuteUpdate();
//
//
//            }
//            catch (Exception e)
//            {
//
//                Log.Error(e.Message);
//            }
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The operation info.</returns>
        public virtual void Save(T entity)
        {

            try
            {
                Session.Set<T>().Add(entity);
                var dbEntityEntries = Session.ChangeTracker.Entries();
                
                Session.SaveChanges();
               // SaveChanges();

            }
            catch (Exception e)
            {
                var dbEntityValidationResults = Session.GetValidationErrors();
                Log.Error(e.Message);
            }
        }

        /// <summary>
        /// Adds the specified entities.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <returns>The operation info.</returns>
        public virtual void Save(IList<T> entities)
        {
            try
            {
                for (int i = 0; i < entities.Count; i++)
                {
                    Session.SaveChanges();
                }

                SaveChanges();

            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }
        }

        /// <summary>
        /// Saves the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The operation info.</returns>
        public void Update(T entity)
        {
//            try
//            {
//                SaveChanges();
//            }
//            catch (Exception e)
//            {
//                Log.Error(e.Message);
//            }

        }

        public void Update(IList<T> entities)
        {
            for (int i = 0; i < entities.Count; i++)
            {
//                Session.Update(entities[i]);
            }

            SaveChanges();
        }

        /// <summary>
        /// Saves all changes made in this context to the underlying database.
        /// </summary>
        public void SaveChanges()
        {
            _session.SaveChanges();
        }

        public void ExecuteUpdate(string hql)
        {
//            try
//            {
//                Session.CreateQuery(hql).ExecuteUpdate();
//            }
//            catch (Exception e)
//            {
//                Log.Error(e.Message);
//            }
        }

        public IEnumerable<T> ExecuteHql(string hql)
        {
            return null;//Session.CreateQuery(hql).SetResultTransformer(new DistinctRootEntityResultTransformer()).List<T>();
        }

        public IEnumerable<T> Query()
        {
            return null;//Session.CreateCriteria<T>().List<T>();
        }

        public void Delete(string query)
        {
//            try
//            {
//                Session.Delete(query);
//                SaveChanges();
//            }
//            catch (Exception e)
//            {
//                Log.Error(e.Message);
//            }

        }

        #endregion

        #region IRepository<T> Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void SetDbContext(object dbContext)
        {
            if (!(dbContext is ISession))
            {
                throw new Exception("Must be a Session object");
            }
            _session = dbContext as DbContext;
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Session.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
