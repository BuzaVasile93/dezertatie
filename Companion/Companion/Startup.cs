﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Companion.Startup))]
namespace Companion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
