﻿using System;
using System.Web.Mvc;
using Companion.Identity;
using CompanionBl.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Companion
{
    public class ApplicationUserManager : UserManager<IdentityUser>
    {
        public ApplicationUserManager(IUserStore<IdentityUser> store)
            : base(store)
        {
            
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            //  ICompanionSecurityService service;
            IdentityUserStore usrStore;

            try
            {
                // service = DependencyResolver.Current.GetService(typeof (ICompanionSecurityService)) as ICompanionSecurityService;
                usrStore = DependencyResolver.Current.GetService(typeof(IdentityUserStore)) as IdentityUserStore;
            }
            catch (Exception e)
            {
                return null;
            }

            var manager = new ApplicationUserManager(usrStore);

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<IdentityUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = false
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 1,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = false;

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<IdentityUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

//    public class ApplicationRoleManager : RoleManager<IdentityRole>
//    {
//        public ApplicationRoleManager(IRoleStore<IdentityRole, string> store) : base(store)
//        {
//        }
//
////        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
////        {
////            var service = DependencyResolver.Current.GetService(typeof(ICompanionSecurityService)) as ICompanionSecurityService;
////            var roleStore = new RoleStore(service);
////            return new ApplicationRoleManager(roleStore);
////        } 
//    }
}