﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompanionBl;
using System.Web.Mvc;
using CompanionBl.Interfaces;

namespace Companion.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        //        private readonly IUserService _userService;
        //        public HomeController(IUserService userService)
        //        {
        //            _userService = userService;
        //        }
       // var t = _userService.GetUsers().ToList();

        public ActionResult Index()
        {
          
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}