using System;
using System.Collections.Generic;

namespace CompanionBl.Models
{
    public partial class NotificationData
    {
        public int RecordId { get; set; }
        public string UserId { get; set; }
        public string ReceptorId { get; set; }
        public string Message { get; set; }
        public int Agreement { get; set; }
    }
}
