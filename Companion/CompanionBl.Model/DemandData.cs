using System;
using System.Collections.Generic;

namespace CompanionBl.Models
{
    public partial class DemandData
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public string UserType { get; set; }
        public string StartingPoint { get; set; }
        public string StopPoint { get; set; }
        public string Message { get; set; }
        public int CarPlace { get; set; }
        public virtual UserData User { get; set; }
    }
}
