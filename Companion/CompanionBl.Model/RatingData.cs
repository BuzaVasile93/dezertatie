using System;
using System.Collections.Generic;

namespace CompanionBl.Models
{
    public partial class RatingData
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public int Rating1 { get; set; }
        public string Comment { get; set; }
    }
}
