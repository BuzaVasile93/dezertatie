using System;
using System.Collections.Generic;

namespace CompanionBl.Models
{
    public partial class MessageData
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public string ReceptorId { get; set; }
        public string UserType { get; set; }
        public string TitleMessage { get; set; }
        public string Message1 { get; set; }
        public System.DateTime Date { get; set; }
        public virtual UserData User { get; set; }
    }
}
