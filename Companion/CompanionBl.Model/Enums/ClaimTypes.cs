﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanionBl.Model.Enums
{
    public enum ClaimTypes
    {
        Root = 00000,
        ArticleManagement = 00001,
        BtnArtEdit = 00100,
        BtnArtAddGridItem = 00101,
        BtnArtPrepareInventory = 00102,
        BtnArtConsultStock = 00103,
        BtnArtExportToCsv = 00104,
        BtnArtSynchronisation = 00105,
        BtnArtCreate = 00106,
        BtnArtMedEdit = 00107,
        BtnArtExtraData = 00108,
        BtnArtDetails = 00109,
        BtnArtMedDelete = 00110
      
    }
}
