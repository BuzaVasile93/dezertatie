﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanionBl.Model.Enums
{
    public enum Role
    {
        DRIVER = 1,
        PASSENGER = 2
    }
}
