﻿using System.Collections.Generic;
using CompanionBl.Models;

namespace CompanionBl.Model
{
    public class RoleData
    {
        public RoleData()
        {
            this.RoleClaims = new List<RoleClaimData>();
            this.Users = new List<UserData>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<RoleClaimData> RoleClaims { get; set; }
        public virtual ICollection<UserData> Users { get; set; }

    }

    public class RoleClaimData
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public virtual RoleData Role { get; set; }

    }
   
}
