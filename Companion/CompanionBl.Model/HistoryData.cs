using System;
using System.Collections.Generic;

namespace CompanionBl.Models
{
    public partial class HistoryData
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public string UserType { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Comment { get; set; }
        public string StartingPoint { get; set; }
        public string StopPoint { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
    }
}
