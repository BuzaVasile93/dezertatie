using System;
using System.Collections.Generic;
using CompanionBl.Model;


namespace CompanionBl.Models
{
    public partial class UserData
    {
        public UserData()
        {
            this.Demands = new List<DemandData>();
            this.Messages = new List<MessageData>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public string CarType { get; set; }
        public string CarNr { get; set; }
        public string Message { get; set; }
        public int Experince { get; set; }
        public string Photo { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public virtual ICollection<DemandData> Demands { get; set; }
        public virtual ICollection<MessageData> Messages { get; set; }
        public ICollection<RoleData> Roles { get; set; } 
    }
}
