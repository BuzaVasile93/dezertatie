﻿namespace Companion.Identity
{
    public class IdentityUserLogin
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

    }
}
