using System.Collections.Generic;
using Microsoft.AspNet.Identity;
namespace Companion.Identity
{
    public class IdentityRole : IRole
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public ICollection<IdentityUser> Users { get; protected set; }
        public ICollection<IdentityUserClaim> Claims { get; set; }

        public IdentityRole()
        {
            Id = "0";
            Users = new List<IdentityUser>();
        }

        public IdentityRole(string roleName)
            : this()
        {
            Name = roleName;
        }
    }
}