using System;
using System.Linq;
using System.Threading.Tasks;
using CompanionBl.Interfaces;
using Microsoft.AspNet.Identity;

namespace Companion.Identity
{
    public class RoleStore : IQueryableRoleStore<IdentityRole>, IRoleStore<IdentityRole>
    {
        private bool _disposed;
        private readonly ICompanionSecurityService _securityService;

        public RoleStore(ICompanionSecurityService securityService)
        {
            _securityService = securityService;
        }

        public virtual Task<IdentityRole> FindByIdAsync(string roleId)
        {
            int id;
            if (!int.TryParse(roleId, out id))
            {
                throw new ArgumentException();
            }

            var dbRole = _securityService.GetRoleById(id);

            if (dbRole == null)
            {
                return Task.FromResult(new IdentityRole());
            }

            var role = new IdentityRole
            {
                Id = roleId, 
                Name = dbRole.Name,
            };

            var roleClaims = dbRole.RoleClaims.Select(
                p => new IdentityUserClaim() {ClaimType = p.ClaimType, ClaimValue = p.ClaimValue, Id = p.Id}).ToList();
            role.Claims = roleClaims;


            return Task.FromResult(role);
        }

        public virtual Task<IdentityRole> FindByNameAsync(string roleName)
        {
            var dbRole = _securityService.GetRoleByName(roleName);

            if (dbRole == null)
            {
                IdentityRole result = null;
                return Task.FromResult(result);
            }
            var role = new IdentityRole { Id = dbRole.Id.ToString(), Name = dbRole.Name };
            return Task.FromResult(role);
        }

        public virtual Task CreateAsync(IdentityRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            int id;
            if (!int.TryParse(role.Id, out id))
            {
                throw new ArgumentException();
            }
            return Task.FromResult(0);
        }

        public virtual Task DeleteAsync(IdentityRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            int id;
            if (!int.TryParse(role.Id, out id))
            {
                throw new ArgumentException();
            }

            _securityService.DeleteRoleById(id);
            return Task.FromResult(0);
        }

        public virtual Task UpdateAsync(IdentityRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            int id;
            if (!int.TryParse(role.Id, out id))
            {
                throw new ArgumentException();
            }
            return Task.FromResult(0);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public IQueryable<IdentityRole> Roles
        {
            get
            {
                var roles = _securityService.ListRoles();
                return roles.Select(role => new IdentityRole
                    {
                        Id = role.Id.ToString(),
                        Name = role.Name,
                        Claims = role.RoleClaims.Select(p=> new IdentityUserClaim(){ClaimType = p.ClaimType,ClaimValue = p.ClaimValue,Id = p.Id}).ToList()
                    }
                ).AsQueryable();
            }
        }
    }


}