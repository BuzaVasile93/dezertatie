﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CompanionBl.Interfaces;
using CompanionBl.Model;
using CompanionBl.Models;
using Microsoft.AspNet.Identity;


namespace Companion.Identity
{
    public class IdentityUserStore : IUserLoginStore<IdentityUser>,
        IUserClaimStore<IdentityUser>,
        IUserRoleStore<IdentityUser>,
        IUserPasswordStore<IdentityUser>,
        IUserSecurityStampStore<IdentityUser>,
        IQueryableUserStore<IdentityUser>,
        IUserStore<IdentityUser>,
        IUserLockoutStore<IdentityUser, string>,
        IUserEmailStore<IdentityUser>,
        IUserPhoneNumberStore<IdentityUser>,
        IUserTwoFactorStore<IdentityUser, string>
    {
        private readonly ICompanionSecurityService _companionSecurityService;
     //   private readonly IDispenserSecurityService _dispenserSecurityService;


        public IdentityUserStore(ICompanionSecurityService companionSecurityService)
        {
            _companionSecurityService = companionSecurityService;
        }

        public void Dispose()
        {

        }

        public Task CreateAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var dbUser = new UserData();
            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.PasswordHash = user.PasswordHash;
            dbUser.Email = user.Email;
            dbUser.PhoneNumber = user.PhoneNumber;
            dbUser.SecurityStamp = user.SecurityStamp;
            dbUser.UserName = user.UserName;

          //  dbUser.Roles = null;// user.Roles;
            //dbUser.Id = Int32.Parse(user.Id);

            //            if (user.Roles.Count != 0)
            //            {
            //                IdentityRole role = user.Role;
            //                dbUser.Role = new Role();
            //                dbUser.RoleId = Convert.ToInt32(role.Id);
            //                dbUser.Role.Name = role.Name;
            //            }

            _companionSecurityService.AddUser(dbUser);

            var u = _companionSecurityService.GetUserByUserName(user.UserName);
           // u.Roles = user.Roles;
            //_companionSecurityService.UpdateUser(u);
            user.Id = u.Id.ToString();
            //            UserRoleData ur = new UserRoleData();
            //            ur.RoleId = Int32.Parse(user.UserType);
            //            ur.UserId = u.Id;
            _companionSecurityService.AddRole(Convert.ToInt32(user.Id), user.Roles.FirstOrDefault().Id);
            return Task.FromResult(0);
        }

        public Task UpdateAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }


            var dbUser = new UserData();
            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.PasswordHash = user.PasswordHash;
            dbUser.Email = user.Email;
            dbUser.PhoneNumber = user.PhoneNumber;

//            if (user.Roles.Count != 0)
//            {
//                IdentityRole role = user.Role;
//                dbUser.Role = new Role();
//                dbUser.RoleId = Convert.ToInt32(role.Id);
//                dbUser.Role.Name = role.Name;
//            }

            _companionSecurityService.UpdateUser(dbUser);

            return Task.FromResult(0);
        }

        public Task DeleteAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var dbUser = new UserData();
           // dbUser.Id = user.Id;
            _companionSecurityService.DeleteUser(dbUser);
            return Task.FromResult(0);
        }

        private List<IdentityUserClaim> CreateAverellClaims()
        {
            var result = new List<IdentityUserClaim>();

            var values = Enum.GetValues(typeof(ClaimTypes));
            foreach (var value in values)
            {
            //    result.Add(new IdentityUserClaim() { ClaimType = ((ClaimTypes)value).ToString(), ClaimValue = value.ToString() });
            }
            return result;
        }

        private IdentityUser CreateDispenserUser(string userId)
        {
//            Guid securityStamp;
//            if (!Guid.TryParse(userId, out securityStamp) || _dispenserSecurityService == null)
//            {
//                return null;
//            }
//
//            var users = _dispenserSecurityService.LoadUsers(p => p.SecurityStamp == userId);
//            var user = users.FirstOrDefault();
//            if (user == null)
//            {
//                return null;
//            }
//
//            var dispenserUser = new IdentityUser();
//            dispenserUser.Email = user.Email;
//            dispenserUser.UserName = user.UserName;
//            dispenserUser.FirstName = user.FirstName;
//            dispenserUser.LastName = user.LastName;
//            dispenserUser.PhoneNumber = user.PhoneNumber;
//            dispenserUser.SecurityStamp = user.SecurityStamp;
//            dispenserUser.Language = Utils.GetLanguage(user.Language.GetValueOrDefault());
//            dispenserUser.Roles.Add(new IdentityRole
//            {
//                Name = Settings.ElearningRoleName,
//                Id = "0",
//                Claims = new List<IdentityUserClaim>()
//            });
//            dispenserUser.Role.Claims.Add(new IdentityUserClaim() { ClaimType = "Elearning", ClaimValue = "16" });
//            dispenserUser.Role.Claims.Add(new IdentityUserClaim() { ClaimType = "ElearningSelfTuition", ClaimValue = "1600" });
//            dispenserUser.Role.Claims.Add(new IdentityUserClaim() { ClaimType = "ElearningTest", ClaimValue = "1601" });
//
//            return dispenserUser;
            return null;
        }
       

        private IdentityUser CreateAvarellUser(string userId)
        {
//            var averell = new IdentityUser
//            {
//                Id = "0",
//                UserName = "averell dalton",
//                PasswordHash = "",
//                Email = "averell.dalton@vanas.eu",
//                SecurityStamp = Guid.NewGuid().ToString(),
//                FirstName = "Averell",
//                LastName = "Dalton",
//                Language = GetDefaultLanguage(),
//                PhoneNumber = "+32 486 99 29 42",
//                UnitRightIds = new List<int>(),
//                AccessAllUnits = true
//            };
//            var role = new IdentityRole
//            {
//                Name = "Averell",
//                Id = "0"
//            };
//
//            role.Claims = CreateAverellClaims();
//            averell.Roles.Add(role);
//
//            return averell;
return null;

        }

        private IdentityUser FindSpecialUser(string userId)
        {
            if (userId == "0")
            {
                return CreateAvarellUser(userId);
            }

            return CreateDispenserUser(userId);
        }

        public Task<IdentityUser> FindByIdAsync(string userId)
        {
            int id;

            if (string.IsNullOrEmpty(userId))
            {
                return Task.FromResult(new IdentityUser());
            }


            if (!int.TryParse(userId, out id) || userId == "0")
            {
                var specialUser = FindSpecialUser(userId);
                if (specialUser == null)
                {
                    throw new ArgumentException("userId");
                }
                return Task.FromResult(specialUser);
            }

            var tmp = _companionSecurityService.GetUserById(userId);

            if (tmp == null)
            {
                return Task.FromResult(new IdentityUser());
            }

            var result = new IdentityUser
            {
                Id = tmp.Id.ToString(),
                PasswordHash = tmp.PasswordHash,
                Email = tmp.Email,
                FirstName = tmp.FirstName,
                LastName = tmp.LastName,
                PhoneNumber = tmp.PhoneNumber,
                SecurityStamp =  tmp.SecurityStamp,
                UserName = tmp.UserName,
                Roles = tmp.Roles
               

            };

//            if (tmp.Role != null)
//            {
//                var role = new IdentityRole
//                {
//                    Name = tmp.Role.Name,
//                    Id = tmp.Role.Id.ToString(),
//                };
//
//                role.Claims =
//                  tmp.Role.Claims.Select(
//                      p => new IdentityUserClaim() { ClaimValue = p.ClaimValue, ClaimType = p.ClaimType, Id = p.Id })
//                      .ToList();
//
//                result.Roles.Add(role);
//            }

            return Task.FromResult(result);
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
           var tmp = _companionSecurityService.GetUserByUserName(userName);
            if (tmp == null)
            {
                return Task.FromResult(new IdentityUser(userName));
            }

            var result = new IdentityUser
            {
                Id = tmp.Id.ToString(),
                PasswordHash = tmp.PasswordHash,
                Email = tmp.Email,
                SecurityStamp = tmp.SecurityStamp,
                FirstName = tmp.FirstName,
                LastName = tmp.LastName,
                PhoneNumber = tmp.PhoneNumber,
                UserName =  tmp.UserName,
                Roles = tmp.Roles
            };

//            if (tmp.Role != null)
//            {
//                var role = new IdentityRole
//                {
//                    Name = tmp.Role.Name,
//                    Id = tmp.Role.Id.ToString(),
//                };
//
//                role.Claims =
//                    tmp.Role.Claims.Select(
//                        p => new IdentityUserClaim() { ClaimValue = p.ClaimValue, ClaimType = p.ClaimType, Id = p.Id })
//                        .ToList();
//                result.Roles.Add(role);
//            }



            return Task.FromResult(result);
         
        }

        public Task AddLoginAsync(IdentityUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }


            return Task.FromResult(0);
        }

        public Task RemoveLoginAsync(IdentityUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            return Task.FromResult(0);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            IList<UserLoginInfo> result = new List<UserLoginInfo>();
            return Task.FromResult(result);
        }

        public Task<IdentityUser> FindAsync(UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<IList<Claim>> GetClaimsAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            IList<Claim> result = (from r in user.Roles from c in r.RoleClaims select new Claim(c.ClaimType, c.ClaimValue)).ToList();
            // var rols = user.Roles.Select( rol => rol.RoleClaims.Select(x => new Claim(x.ClaimType, x.ClaimValue)));

          

            return Task.FromResult(result);

        }

        public Task AddClaimAsync(IdentityUser user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

//            user.Claims.Add(new IdentityUserClaim
//            {
//                User = user,
//                ClaimType = claim.Type,
//                ClaimValue = claim.ValueIsInRoleAsync
//            });

            return Task.FromResult(0);
          
        }

        public Task RemoveClaimAsync(IdentityUser user, Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

//            foreach (IdentityUserClaim identityUserClaim in user.Claims.Where(uc =>
//            {
//                if (uc.ClaimValue == claim.Value)
//                {
//                    return uc.ClaimType == claim.Type;
//                }
//                return false;
//            }).ToList())
//            {
//                user.Claims.Remove(identityUserClaim);
//            }

            return Task.FromResult(0);
        }

        public Task AddToRoleAsync(IdentityUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("role");
            }

            IdentityRole identityRole = new IdentityRole(roleName);
           // user.Roles.Add(identityRole);

            return Task.FromResult(0);
        }

        public Task RemoveFromRoleAsync(IdentityUser user, string roleName)
        {
//            if (user == null)
//            {
//                throw new ArgumentNullException("user");
//            }
//            if (string.IsNullOrWhiteSpace(roleName))
//            {
//                throw new ArgumentException("role");
//            }
//
//            IdentityRole identityUserRole = user.Roles.FirstOrDefault(r => r.Name.ToUpper() == roleName.ToUpper());
//            if (identityUserRole != null)
//            {
//                user.Roles.Remove(identityUserRole);
//            }
//
//            return Task.FromResult(0);
            return null; 
        }

        public Task<IList<string>> GetRolesAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult(user.Roles.Select(u => u.Name).ToList() as IList<string>);
        }

        public Task<bool> IsInRoleAsync(IdentityUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("role");
            }

            return Task.FromResult(user.Roles.Any(r => r.Name.ToUpper() == roleName.ToUpper()));
            //return null;
        }

        public Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(IdentityUser user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public Task SetSecurityStampAsync(IdentityUser user, string stamp)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public Task<string> GetSecurityStampAsync(IdentityUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult(user.SecurityStamp);
        }

        public IQueryable<IdentityUser> Users
        {
            get
            {

                var tmp = _companionSecurityService.ListUsers();
                var identityUsers = tmp.Select(user => new IdentityUser
                {
                    Id = user.Id.ToString(),
                    PasswordHash = user.PasswordHash,
                    Email = user.Email,
                   
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber
                }
                        ).ToList();
                return identityUsers.AsQueryable();
            }
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUser user)
        {
            return Task.FromResult(DateTimeOffset.MinValue);
        }

        public Task SetLockoutEndDateAsync(IdentityUser user, DateTimeOffset lockoutEnd)
        {
//            return Task.CompletedTask;
            return null;
        }

        public Task<int> IncrementAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(1);
        }

        public Task ResetAccessFailedCountAsync(IdentityUser user)
        {
//            return Task.CompletedTask;
            return null;
        }

        public Task<int> GetAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(0);
        }

        public Task<bool> GetLockoutEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(false);
        }

        public Task SetLockoutEnabledAsync(IdentityUser user, bool enabled)
        {
//            return Task.CompletedTask;
            return null;
        }

        public Task SetEmailAsync(IdentityUser user, string email)
        {
//            int id;
//            if (!int.TryParse(user.Id, out id))
//            {
//                throw new ArgumentException("user");
//            }
//
//            user.Email = email;
//            var dbUser = _pilotSecurityService.GetUserById(id);
//            dbUser.Email = email;
//            _pilotSecurityService.UpdateUser(dbUser);
//            return Task.FromResult(0);
            return null;
        }

        public Task<string> GetEmailAsync(IdentityUser user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(true);
        }

        public Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed)
        {
            return Task.FromResult(0);
        }

        public Task<IdentityUser> FindByEmailAsync(string email)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumberAsync(IdentityUser user, string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPhoneNumberAsync(IdentityUser user)
        {
            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(true);
        }

        public Task SetPhoneNumberConfirmedAsync(IdentityUser user, bool confirmed)
        {
            return Task.FromResult(true);
        }

        public Task SetTwoFactorEnabledAsync(IdentityUser user, bool enabled)
        {
            return Task.FromResult(false);
        }

        public Task<bool> GetTwoFactorEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(false);
        }
    }
}