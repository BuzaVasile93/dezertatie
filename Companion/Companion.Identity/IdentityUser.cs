using System.Collections.Generic;
using System.Security.Claims;
using CompanionBl.Model;
using Microsoft.AspNet.Identity;

namespace Companion.Identity
{
    public class IdentityUser : IUser<string>
    {
        public IdentityUser(string userName)
        {
            UserName = userName;
        }

        public IdentityUser()
        {
            Roles = new List<RoleData>();
            UnitRightIds = new List<int>();
        }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public string Adress { get; set; }
        public string LockoutEndDateUtc { get; set; }
        public string SecurityStamp { get; set; }
        public string UserType { get; set; }

        public ICollection<RoleData> Roles { get; set; }
        
        public List<Claim> Claims { get; set; }
             
        public List<int> UnitRightIds { get; set; }
        
             
    }
}
    