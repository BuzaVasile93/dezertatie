using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
namespace Companion.Identity
{
    public class IdentityUserClaim
    {
        public int Id { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public IdentityUser User { get; set; }

    }

  

    public class ClaimsAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly List<string> _claimTypes;

        public ClaimsAuthorizeAttribute(string type)
        {
            this._claimTypes = new List<string>();
            this._claimTypes.Add(type);
        }

        public ClaimsAuthorizeAttribute(string[] types)
        {
            this._claimTypes = new List<string>();
            this._claimTypes.AddRange(types);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            if (user == null)
            {
                base.HandleUnauthorizedRequest(filterContext);
                return;
            }
            var claims = user.Claims.Where(p => this._claimTypes.Contains(p.Type));

            var blnArtManagement = _claimTypes.Contains("ArticleManagement");
            var blnLocManagement = _claimTypes.Contains("LocationManagement");

            var f = new Func<Claim, bool>(p => _claimTypes.Contains(p.Type));

            var fbtnArt = new Func<Claim, bool>(p => p.Type.StartsWith("BtnArt"));
            var fbtnLoc = new Func<Claim, bool>(p => p.Type.StartsWith("BtnLoc"));

            var f1 = f;
            var fArt = new Func<Claim, bool>(p => f1(p) || fbtnArt(p));
            var f2 = f;
            var fLoc = new Func<Claim, bool>(p => f2(p) || fbtnLoc(p));

            if (blnLocManagement && !blnArtManagement)
            {
                f = fLoc;
            }

            if (!blnLocManagement && blnArtManagement)
            {
                f = fArt;
            }

            if (blnArtManagement && blnLocManagement)
            {
                f = p => fArt(p) || fLoc(p);

            }

            if (blnArtManagement || blnLocManagement)
            {
                claims = user.Claims.Where(f);
            }

            if (claims.Any())
            {
                base.OnAuthorization(filterContext);
            }
            else
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}