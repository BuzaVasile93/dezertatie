using System;
using System.Collections.Generic;

namespace CompanionDal.Models
{
    public partial class Notification
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public int ReceptorId { get; set; }
        public string Message { get; set; }
        public int Agreement { get; set; }
        public virtual User User { get; set; }
    }
}
