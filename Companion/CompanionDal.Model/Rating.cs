using System;
using System.Collections.Generic;

namespace CompanionDal.Models
{
    public partial class Rating
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> Rating1 { get; set; }
        public string Comment { get; set; }
    }
}
