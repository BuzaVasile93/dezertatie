using System;
using System.Collections.Generic;

namespace CompanionDal.Models
{
    public partial class User
    {
        public User()
        {
            this.Demands = new List<Demand>();
            this.Messages = new List<Message>();
            this.Notifications = new List<Notification>();
            this.Roles = new List<Role>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public string UserName { get; set; }
        public string CarType { get; set; }
        public string CarNr { get; set; }
        public string Message { get; set; }
        public Nullable<int> Experince { get; set; }
        public string Photo { get; set; }
        public virtual ICollection<Demand> Demands { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
