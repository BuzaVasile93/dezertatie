using System;
using System.Collections.Generic;

namespace CompanionDal.Models
{
    public partial class Message
    {
        public int RecordId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> ReceptorId { get; set; }
        public string UserType { get; set; }
        public string TitleMessage { get; set; }
        public string Message1 { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public virtual User User { get; set; }
    }
}
