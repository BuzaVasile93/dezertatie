using System;
using System.Collections.Generic;

namespace CompanionDal.Models
{
    public partial class Role
    {
        public Role()
        {
            this.RoleClaims = new List<RoleClaim>();
            this.Users = new List<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<RoleClaim> RoleClaims { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
