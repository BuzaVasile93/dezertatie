﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.Interfaces;
using CompanionDal.Interfaces;
using CompanionDal.Models;

namespace CompanionBl
{
    public class RatingService : ServiceBase, IRatingService
    {
        private readonly IRatingRepository _ratingService;
        public RatingService(IRatingRepository ratingService, ILogger logger) : base(logger)
        {
            _ratingService = ratingService;
        }
    }
}
