﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.Interfaces;
using CompanionDal.Interfaces;
using CompanionDal.Models;

namespace CompanionBl
{
    public class DemandService : ServiceBase, IDemandService
    {
        private readonly IDemandRepository _demandService;
        public DemandService(IDemandRepository demandService, ILogger logger) : base(logger)
        {
            _demandService = demandService;
        }
    }
}
