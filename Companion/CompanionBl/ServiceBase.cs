﻿using Companion.Log;
using CompanionBl.Interfaces;

namespace CompanionBl
{
   public class ServiceBase: IServiceBase
    {
        protected ILogger _logger;

        protected ServiceBase(ILogger logger)
        {
            _logger = logger;
        }
    }
}
