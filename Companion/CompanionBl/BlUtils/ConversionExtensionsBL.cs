﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CompanionBl.Model;
using CompanionBl.Models;
using CompanionDal.Models;

namespace CompanionBl.BlUtils
{
    public static class ConversionExtensionsBL
    {
        public static User ConvertToUser(this UserData BlData)
        {
            if (BlData == null)
            {
                return null;
            }
            var DalData = new User();
            {
                DalData.Id = BlData.Id;
                DalData.FirstName = BlData.FirstName;
                DalData.LastName = BlData.LastName;
                DalData.UserName = BlData.UserName;
                DalData.Address = BlData.Address;
                DalData.Email = BlData.Email;
                DalData.PasswordHash = BlData.PasswordHash;
                DalData.SecurityStamp = BlData.SecurityStamp;
                if (BlData.Roles != null)
                {
                    DalData.Roles = BlData.Roles.Select(x => x.ConvertToRole()).ToList();
                }
               
//                DalData.Demands = BlData.Demands.Select(x => x.ConvertToDemand()).ToList();
//                DalData.Messages = BlData.Messages.Select(x => x.ConvertToMessage()).ToList();
            };
            return DalData;
        }

        public static UserData ConvertToUserData(this User DalData)
        {
            if (DalData == null)
            {
                return null;
            }
            var BlData = new UserData();
            {
                BlData.Id = DalData.Id;
                BlData.FirstName = DalData.FirstName;
                BlData.LastName = DalData.LastName;
                BlData.UserName = DalData.UserName;
                BlData.Address = DalData.Address;
                BlData.Email = DalData.Email;
                BlData.PasswordHash = DalData.PasswordHash;
                BlData.SecurityStamp = DalData.SecurityStamp;
                BlData.Roles = DalData.Roles.Select(x => x.ConvertToRoleData()).ToList();
                
                //                BlData.Demands = DalData.Demands.Select(x => x.ConvertToDemandData()).ToList();
                //                BlData.Messages = DalData.Messages.Select(x => x.ConvertToMessageData()).ToList();
            };
            return BlData;
        }

        public static Role ConvertToRole(this RoleData BlData)
        {
            if (BlData == null)
            {
                return null;
            }
            var DalData = new Role();
            {
                DalData.Id = BlData.Id;
                DalData.Name = BlData.Name;
                DalData.RoleClaims = BlData.RoleClaims.Select(x => x.ConvertToRoleClaim()).ToList();
            };
            return DalData;
        }

        public static RoleData ConvertToRoleData (this Role DalData)
        {
            if (DalData == null)
            {
                return null;
            }
            var BlData = new RoleData();
            {
                BlData.Id = DalData.Id;
                BlData.Name = DalData.Name;
                BlData.RoleClaims = DalData.RoleClaims.Select(x => x.ConvertToRoleClaimData()).ToList();
            };
            return BlData;
        }

        public static RoleClaim ConvertToRoleClaim(this RoleClaimData BlData)
        {
            if (BlData == null)
            {
                return null;
            }
            var DalData = new RoleClaim();
            {
                DalData.Id = BlData.Id;
                DalData.ClaimType = BlData.ClaimType;
                DalData.ClaimValue = BlData.ClaimValue;
                DalData.RoleId = BlData.RoleId;
                //DalData.Role = BlData.Role.ConvertToRole();
            };
            return DalData;
        }

        public static RoleClaimData ConvertToRoleClaimData(this RoleClaim DalData)
        {
            if (DalData == null)
            {
                return null;
            }
            var BlData = new RoleClaimData();
            {
                BlData.Id = DalData.Id;
                BlData.ClaimType = DalData.ClaimType;
                BlData.ClaimValue = DalData.ClaimValue;
                BlData.RoleId = DalData.RoleId;
               // BlData.Role = DalData.Role.ConvertToRoleData();
            };
            return BlData;
        }

      



        //
        //        public static Demand ConvertToDemand(this DemandData BlData)
        //        {
        //            if (BlData == null)
        //            {
        //                return null;
        //            }
        //            var DalData = new Demand();
        //            {
        //                DalData.RecordId = BlData.RecordId;
        //                DalData.UserId = BlData.UserId;
        //                DalData.UserType = BlData.UserType;
        //                DalData.StartingPoint = BlData.StartingPoint;
        //                DalData.StopPoint = BlData.StopPoint;
        //                DalData.Message = BlData.Message;
        //                DalData.CarPlace = BlData.CarPlace;
        //                DalData.User = BlData.User.ConvertToUser();
        //
        //            };
        //            return DalData;
        //        }
        //
        //        public static DemandData ConvertToDemandData(this Demand DalData)
        //        {
        //            if (DalData == null)
        //            {
        //                return null;
        //            }
        //            var BlData = new DemandData();
        //            {
        //                BlData.RecordId = BlData.RecordId;
        //                BlData.UserId = DalData.UserId;
        //                BlData.UserType = DalData.UserType;
        //                BlData.StartingPoint = DalData.StartingPoint;
        //                BlData.StopPoint = DalData.StopPoint;
        //                BlData.Message = DalData.Message;
        //                BlData.CarPlace = DalData.CarPlace;
        //                BlData.User = DalData.User.ConvertToUserData();
        //
        //            };
        //            return BlData;
        //        }
        //
        //        public static History ConvertToHistory(this HistoryData BlData)
        //        {
        //            if (BlData == null)
        //            {
        //                return null;
        //            }
        //            var DalData = new History();
        //            {
        //                DalData.RecordId = BlData.RecordId;
        //                DalData.UserId = BlData.UserId;
        //                DalData.UserType = BlData.UserType;
        //                DalData.Rating = BlData.Rating;
        //                DalData.Comment = BlData.Comment;
        //                DalData.StartingPoint = BlData.StartingPoint;
        //                DalData.StopPoint = BlData.StopPoint;
        //                DalData.Adress = BlData.Adress;
        //                DalData.PhoneNumber = BlData.PhoneNumber;
        //            };
        //            return DalData;
        //        }

        //        public static HistoryData ConvertToHistoryData(this History DalData)
        //        {
        //            if (DalData == null)
        //            {
        //                return null;
        //            }
        //            var BlData = new HistoryData();
        //            {
        //                BlData.RecordId = DalData.RecordId;
        //                BlData.UserId = DalData.UserId;
        //                BlData.UserType = DalData.UserType;
        //                BlData.Rating = DalData.Rating;
        //                BlData.Comment = DalData.Comment;
        //                BlData.StartingPoint = DalData.StartingPoint;
        //                BlData.StopPoint = DalData.StopPoint;
        //                BlData.Adress = DalData.Adress;
        //                BlData.PhoneNumber = DalData.PhoneNumber;
        //            };
        //            return BlData;
        //        }
        //
        //        public static Message ConvertToMessage(this MessageData BlData)
        //        {
        //            if (BlData == null)
        //            {
        //                return null;
        //            }
        //            var DalData = new Message();
        //            {
        //                DalData.RecordId = BlData.RecordId;
        //                DalData.UserId = BlData.UserId;
        //                DalData.ReceptorId = BlData.ReceptorId;
        //                DalData.UserType = BlData.UserType;
        //                DalData.TitleMessage = BlData.TitleMessage;
        //                DalData.Message1 = BlData.Message1;
        //                DalData.Date = BlData.Date;
        //                DalData.User = BlData.User.ConvertToUser();
        //            };
        //            return DalData;
        //        }
        //
        //        public static MessageData ConvertToMessageData(this Message DalData)
        //        {
        //            if (DalData == null)
        //            {
        //                return null;
        //            }
        //            var BlData = new MessageData();
        //            {
        //                BlData.RecordId = DalData.RecordId;
        //                BlData.UserId = DalData.UserId;
        //                BlData.ReceptorId = DalData.ReceptorId;
        //                BlData.UserType = DalData.UserType;
        //                BlData.TitleMessage = DalData.TitleMessage;
        //                BlData.Message1 = DalData.Message1;
        //                BlData.Date = DalData.Date;
        //                BlData.User = DalData.User.ConvertToUserData();
        //            };
        //            return BlData;
        //        }
        //
        //        public static Notification ConvertToNotification(this NotificationData BlData)
        //        {
        //            if (BlData == null)
        //            {
        //                return null;
        //            }
        //            var DalData = new Notification();
        //            {
        //                DalData.RecordId = BlData.RecordId;
        //                DalData.ReceptorId = BlData.ReceptorId;
        //                DalData.UserId = BlData.UserId;
        //                DalData.Message = BlData.Message;
        //                DalData.Agreement = BlData.Agreement;
        //            };
        //            return DalData;
        //        }
        //
        //        public static NotificationData ConvertToNotificationData(this Notification DalData)
        //        {
        //            if (DalData == null)
        //            {
        //                return null;
        //            }
        //            var BlData = new NotificationData();
        //            {
        //                BlData.RecordId = DalData.RecordId;
        //                BlData.ReceptorId = DalData.ReceptorId;
        //                BlData.UserId = DalData.UserId;
        //                BlData.Message = DalData.Message;
        //                BlData.Agreement = DalData.Agreement;
        //            };
        //            return BlData;
        //        }
        //
        //        public static Rating ConvertToRating(this RatingData BlData)
        //        {
        //            if (BlData == null)
        //            {
        //                return null;
        //            }
        //            var DalData = new Rating();
        //            {
        //
        //                DalData.RecordId = BlData.RecordId;
        //                DalData.UserId = BlData.UserId;
        //                DalData.Rating1 = BlData.Rating1;
        //                DalData.Comment = BlData.Comment;
        //
        //            };
        //            return DalData;
        //        }
        //
        //        public static RatingData ConvertToRatingData(this Rating DalData)
        //        {
        //            if (DalData == null)
        //            {
        //                return null;
        //            }
        //            var BlData = new RatingData();
        //            {
        //                BlData.RecordId = DalData.RecordId;
        //                BlData.UserId = DalData.UserId;
        //                BlData.Rating1 = DalData.Rating1;
        //                BlData.Comment = DalData.Comment;
        //            };
        //            return BlData;
        //        }

    }
}
