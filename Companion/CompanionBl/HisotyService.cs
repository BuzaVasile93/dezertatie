﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.Interfaces;
using CompanionDal.Interfaces;
using CompanionDal.Models;

namespace CompanionBl
{
    public class HisotyService : ServiceBase, IHistoryService
    {
        private readonly IHistoryRepository _historyService;
        public HisotyService(IHistoryRepository historyService, ILogger logger) : base(logger)
        {
            _historyService = historyService;
        }
    }
}
