﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.BlUtils;
using CompanionBl.Interfaces;
using CompanionBl.Model;
using CompanionBl.Models;
using CompanionDal.Interfaces;

namespace CompanionBl
{
    public class RoleService : ServiceBase, IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository, ILogger logger) : base(logger)
        {
            _roleRepository = roleRepository;
        }

        public List<RoleData> GetAllRoles()
        {
            List<RoleData> allRoles;
            allRoles =  _roleRepository.GetAll().Select(x => x.ConvertToRoleData()).ToList();
            if (allRoles.Count == 0)
            {
                allRoles.Add(new RoleData() {Name = "TEST"});
            }
            return allRoles;
        }
    }
}
