﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.Interfaces;
using CompanionDal.Interfaces;
using CompanionDal.Models;

namespace CompanionBl
{
    public class NotificationService : ServiceBase, INotificationService
    {
        private readonly INotificationRepository _notificationService;
        public NotificationService(INotificationRepository notificationService, ILogger logger) : base(logger)
        {
            _notificationService = notificationService;
        }
    }
}
