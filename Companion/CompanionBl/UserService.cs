﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.BlUtils;
using CompanionBl.Interfaces;
using CompanionBl.Models;
using CompanionDal.Interfaces;
using CompanionDal.Models;

namespace CompanionBl
{
    public class UserService : ServiceBase, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository, ILogger logger) : base(logger)
        {
            _userRepository = userRepository;
        }

        public List<UserData> GetUsers()
        {
          return _userRepository.GetAll().Select(x => x.ConvertToUserData()).ToList();
        }

       
    }
}
