﻿using System.Collections.Generic;
using System.Linq;
using Companion.Log;
using CompanionBl.Interfaces;
using CompanionDal.Interfaces;
using CompanionDal.Models;

namespace CompanionBl
{
    public class MessageService : ServiceBase, IMessageService
    {
        private readonly IMessageRepository _messageService;
        public MessageService(IMessageRepository messageService, ILogger logger) : base(logger)
        {
            _messageService = messageService;
        }
    }
}
