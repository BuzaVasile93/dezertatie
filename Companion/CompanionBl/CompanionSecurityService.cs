﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Companion.Log;
using CompanionBl.BlUtils;
using CompanionBl.Interfaces;
using CompanionBl.Model;
using CompanionBl.Models;
using CompanionDal.Interfaces;
using CompanionDal.Models;
using CompanionUtils;

namespace CompanionBl
{
    public class CompanionSecurityService : ServiceBase, ICompanionSecurityService
    {
        private readonly IUserRepository _userRepository;



        public CompanionSecurityService(ILogger logger, IUserRepository userRepository)
            : base(logger)
        {
            _userRepository = userRepository;

        }


        private CompanionSecurityService(ILogger logger) : base(logger)
        {
        }

        public void AddRole(int UserId, int RoleId)
        {
            try
            {
                using (IDbConnection connection = ConnectionFactory.CreateConnection())
                {
                    SqlConnection conn = (SqlConnection)connection;
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO UserRoles (UserId, RoleId) VALUES (@UserID, @RoleId)", conn))
                    {
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        cmd.Parameters.AddWithValue("@RoleId", RoleId);
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }



        }



        public void AddUserToRoles(UserData user)
        {

        }

        public RoleData GetRoleById(int id)
        {
            throw new NotImplementedException();
        }

        public UserData GetUserById(string id)
        {
            int userId = Int32.Parse(id);
            return _userRepository.FirstOrDefault(x => x.Id == userId).ConvertToUserData();
        }

        public void AddUser(UserData user)
        {
            var dbUser = user.ConvertToUser();
            //            
            //            dbUser.PilotRole = _roleRepository.GetById(user.RoleId);
            //            dbUser.Language = GetDbLanguage(user.Language);
            //            dbUser.FirstName = user.FirstName;
            //            dbUser.LastName = user.LastName;
            //            dbUser.Email = user.Email;
            //            dbUser.PhoneNumber = user.PhoneNumber;
            //            dbUser.UserName = user.UserName;
            //            dbUser.PasswordHash = user.PasswordHash;
            //            dbUser.SecurityStamp = user.SecurityStamp;
            //            dbUser.AccessAllUnits = user.AccessAllUnits;
            //
            //            foreach (var unitRoleId in user.UnitRightIds)
            //            {
            //                dbUser.UnitRight.Add(new Unit
            //                {
            //                    RecordId = unitRoleId
            //                });
            //            }
            try
            {
                _userRepository.Save(dbUser);

            }
            catch
            {
            }

        }

        public void UpdateUser(UserData user)
        {
            //            var dbUser = user.ConvertToUser();
            //
            //            try
            //            {
            //             
            //                _userRepository.Save(dbUser);
            //            }
            //            catch
            //            {
            //            }

        }

        public void UpdateRole(RoleData role)
        {
            throw new NotImplementedException();
        }

        public void SaveRoleClaims(RoleData role)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(UserData user)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(User user)
        {
            throw new NotImplementedException();
        }

        public void DeleteUserById(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteRole(RoleData role)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RoleData> ListRoles(Expression<Func<RoleData, bool>> filter = null)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserData> ListUsers(Expression<Func<UserData, bool>> filter = null)
        {
            var result = _userRepository.AsQueryable().OrderBy(u => u.LastName).ThenBy(u => u.FirstName).Select(dbUser => new UserData
            {
                //                Role = new Role
                //                {
                //                    Name = dbUser.PilotRole.RoleName
                //                },
                Email = dbUser.Email,
                // Id = dbUser.RecordId,

                PhoneNumber = dbUser.PhoneNumber,
                //  UserName = dbUser.UserName,
                FirstName = dbUser.FirstName,
                LastName = dbUser.LastName

            });

            //            if (filter != null)
            //            {
            //                result = result.Where(filter);
            //            }
            return result.ToList();
        }

        public IEnumerable<User> ListUsers(Expression<Func<User, bool>> filter = null)
        {
            throw new NotImplementedException();
        }

        public UserData GetUserByUserName(string UserName)
        {
            var user = _userRepository.AsQueryable(p => p.UserName == UserName).FirstOrDefault().ConvertToUserData();
            if (user == null)
            {
                return null;
            }

            var result = new UserData();
            //            result.Role = new Role();
            //            result.Role.Name = pilotUser.PilotRole.RoleName;
            //            result.Role.Id = pilotUser.PilotRole.RecordId;
            result.Id = user.Id;
            result.Email = user.Email;
            result.UserName = user.UserName;
            result.PhoneNumber = user.PhoneNumber;
            result.SecurityStamp = user.SecurityStamp;
            result.FirstName = user.FirstName;
            result.LastName = user.LastName;
            result.PasswordHash = user.PasswordHash;
            result.Roles = user.Roles;
            return result;
        }

        public RoleData GetRoleByName(string roleName)
        {
            throw new NotImplementedException();
        }

        public void DeleteRoleById(int id)
        {
            throw new NotImplementedException();
        }

        public bool ValidateDeleteUser(int userId)
        {
            throw new NotImplementedException();
        }
    }
}
