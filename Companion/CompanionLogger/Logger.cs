using System;
using System.Text;
using log4net;
using log4net.Config;

namespace Companion.Log
{
    public class Logger : ILogger
    {
        private const string LogName = "Companion";
        private readonly ILog _log;

        public Logger()
        {
            _log = LogManager.GetLogger(LogName);
            XmlConfigurator.Configure();

            if (_log == null)
            {
                throw new Exception("Get logger exception");
            }
        }

        private Logger(string logName)
        {
            logName = string.IsNullOrEmpty(logName) ? LogName : logName;
            _log = LogManager.GetLogger(logName);
            XmlConfigurator.Configure();

            if (_log == null)
            {
                throw new Exception("Get logger exception");
            }
        }

        #region Public Methods

        public void Write(string message)
        {
            Write(message, LoggerLevel.ERROR);
        }

        public void Write(string message, LoggerLevel level)
        {
            switch (level)
            {
                case LoggerLevel.DEBUG:
                    Debug(message);
                    break;
                case LoggerLevel.INFO:
                    Info(message);
                    break;
                case LoggerLevel.WARN:
                    Warn(message);
                    break;
                case LoggerLevel.ERROR:
                    Error(message);
                    break;
                case LoggerLevel.FATAL:
                    Fatal(message);
                    break;
                default:
                    break;
            }
        }

        public void Debug(string message)
        {
            if (_log.IsDebugEnabled)
            {
                _log.Debug(message);
            }
        }

        public void Error(string message)
        {
            if (_log.IsErrorEnabled)
            {
                _log.Error(message);
            }
        }

        public void Error(Exception e)
        {
            if (_log.IsErrorEnabled)
            {
                string msg = e.Message + Environment.NewLine
                             + "----------------------------------------" + Environment.NewLine
                             + FullStackTrace(e);
                _log.Error(msg);
            }
        }

        public void Fatal(string message)
        {
            if (_log.IsFatalEnabled)
            {
                _log.Fatal(message);
            }
        }

        public void Info(string message)
        {
            if (_log.IsInfoEnabled)
            {
                _log.Info(message);
            }
        }

        public void Warn(string message)
        {
            if (_log.IsWarnEnabled)
            {
                _log.Warn(message);
            }
        }

        #endregion

        private static string FullStackTrace(Exception e)
        {
            if (e == null || string.IsNullOrEmpty(e.StackTrace))
            {
                return "";
            }
            var sb = new StringBuilder();
            sb.AppendLine(e.GetType().ToString());
            sb.AppendLine(e.Message);
            sb.AppendLine(e.StackTrace);
            if (e.Data.Count > 0)
            {
                foreach (object key in e.Data.Keys)
                {
                    sb.Append("\t").Append(key).Append(" = ").AppendLine("" + e.Data[key]);
                }
            }
            if (e.InnerException != null)
            {
                sb.AppendLine();
                sb.AppendLine(FullStackTrace(e.InnerException));
            }
            return sb.ToString();
        }
    }
}