﻿using System;

namespace Companion.Log
{
    public interface ILogger
    {
        void Write(string message);

        void Write(string message, LoggerLevel level);

        void Debug(string message);

        void Error(string message);

        void Error(Exception exc);

        void Fatal(string message);

        void Info(string message);

        void Warn(string message);
    }
}
