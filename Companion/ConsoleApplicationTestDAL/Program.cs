﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Companion.Log;
using CompanionDAL.Repositories;
using Pharmalogic.Pilot.Dal.Base;
using Pharmalogic.Pilot.Dal.Interfaces.Base;

namespace ConsoleApplicationTestDAL
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnitOfWork i = new UnitOfWork();
            ILogger l = new Logger();
            UserRepository driv = new UserRepository(i, l);

            // act  
            var o=driv.GetAll();
        }
    }
}
