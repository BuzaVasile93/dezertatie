﻿

using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace CompanionUtils
{
    public static class Extensions
    {
      

        public static bool HasClaimRole(this HtmlHelper obj, string claim)
        {
                if (HttpContext.Current.User == null)
                {
                    return false;
                }

                var user = HttpContext.Current.User as ClaimsPrincipal;
                if (user == null)
                {
                    return false;
                }

                var exist = user.Claims.Any(p => p.Type == ClaimTypes.Actor & p.Value == claim);

            return exist;
        }
    }
}
