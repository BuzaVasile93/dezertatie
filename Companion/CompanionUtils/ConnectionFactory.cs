﻿

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace CompanionUtils
{
    public class ConnectionFactory
    {
        public static IDbConnection CreateConnection()
        {
            IDbConnection result = new SqlConnection(Settings.ConnectionString);
            return result;
        }
    }
}