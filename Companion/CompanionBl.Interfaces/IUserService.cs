﻿
using System.Collections.Generic;
using CompanionBl.Models;
using CompanionDal.Models;

namespace CompanionBl.Interfaces
{
    public interface IUserService: IServiceBase
    {
        List<UserData> GetUsers();
    }

}
