﻿
using System.Collections.Generic;
using CompanionBl.Model;
using CompanionBl.Models;

namespace CompanionBl.Interfaces
{
    public interface IRoleService: IServiceBase
    {
        List<RoleData> GetAllRoles();
    }
}
