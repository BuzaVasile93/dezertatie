﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CompanionBl.Model;
using CompanionBl.Models;
using CompanionDal.Models;

namespace CompanionBl.Interfaces
{
    public interface ICompanionSecurityService : IServiceBase
    {
       // [MethodInterceptor(ManipulationKey = ManipulationKeys.AddRole, Behaviour = InterceptorBehaviour.Before)]
        void AddRole(int UserId, int RoleId);

        void AddUserToRoles(UserData user);

        RoleData GetRoleById(int id);
        UserData GetUserById(string id);

       // [MethodInterceptor(ManipulationKey = ManipulationKeys.AddUser, Behaviour = InterceptorBehaviour.Before)]
        void AddUser(UserData user);

      //  [MethodInterceptor(ManipulationKey = ManipulationKeys.UpdateUser, Behaviour = InterceptorBehaviour.Before)]
        void UpdateUser(UserData user);

      //  [MethodInterceptor(ManipulationKey = ManipulationKeys.UpdateRole, Behaviour = InterceptorBehaviour.Before)]
        void UpdateRole(RoleData role);

      //  [MethodInterceptor(ManipulationKey = ManipulationKeys.UpdateRoleRights, Behaviour = InterceptorBehaviour.Before)]
        void SaveRoleClaims(RoleData role);

     //   [MethodInterceptor(ManipulationKey = ManipulationKeys.DeleteUser, Behaviour = InterceptorBehaviour.Before)]
        void DeleteUser(UserData user);

        void DeleteUserById(int id);

      //  [MethodInterceptor(ManipulationKey = ManipulationKeys.DeleteRole, Behaviour = InterceptorBehaviour.Before)]
        void DeleteRole(RoleData role);

        IEnumerable<RoleData> ListRoles(Expression<Func<RoleData, bool>> filter = null);
        IEnumerable<UserData> ListUsers(Expression<Func<UserData, bool>> filter = null);
        UserData GetUserByUserName(string userName);
        RoleData GetRoleByName(string roleName);
   //     [MethodInterceptor(ManipulationKey = ManipulationKeys.DeleteRole, Behaviour = InterceptorBehaviour.Before)]
        void DeleteRoleById(int id);

        bool ValidateDeleteUser(int userId);
    }
}
